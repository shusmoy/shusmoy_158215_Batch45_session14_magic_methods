<?php

namespace App;


class calculator
{

    private $number1;
    private $number2;
    private $operation;
    private $result;

    public function setNumber1($number1)
    {
        $this->number1 = $number1;
    }

    public function setNumber2($number2)
    {
        $this->number2 = $number2;
    }

    public function setOperation($operation)
    {
        $this->operation = $operation;
    }

    public function setResult()
    {
        switch($this -> operation){

            case "Addition":
                $this -> result= $this -> doaddition();
                break;
            case "subtraction":
                $this -> result = $this -> dosubtraction();
                break;
            case "multiplication":
                $this -> result = $this -> domultiplication();
                break;
            case "division":
                $this -> result = $this -> dodivision();
                break;
        }
    }

    public function getNumber1()
    {
        return $this->number1;
    }

    public function getNumber2()
    {
        return $this->number2;
    }

    public function getOperation()
    {
        return $this->operation;
    }

    public function getResult()
    {
        $this ->setResult();
        return $this->result;
    }

    private function doaddition(){
        return $this -> number1 + $this -> number2;
    }


    private function dosubtraction(){
        return $this -> number1 - $this -> number2;
    }

    private function domultiplication(){
    return $this -> number1 * $this -> number2;
    }


    private function dodivision(){
    return $this -> number1 / $this -> number2;
}
}