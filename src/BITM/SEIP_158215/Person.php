<?php
namespace App;


class Person
{

    private $name;
    private $dateOfBirth;


    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDateofbirth($dateOfBirth)
    {
        $this->dateofbirth = $dateOfBirth;
    }

    public function getDateofbirth()
    {
        return $this->dateOfBirth;
    }
}